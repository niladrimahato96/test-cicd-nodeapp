const request = require('supertest');
const app = require('../app');

describe('GET /active', () => {
  it('should respond with Hello, world!', async () => {
    const response = await request(app).get('/active');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Hello, world!');
  });
});

describe('GET /error', () => {
  it('should respond with Internal Server Error', async () => {
    const response = await request(app).get('/error');
    expect(response.status).toBe(500);
    expect(response.text).toBe('Internal Server Error.');
  });
});

describe('GET /sum', () => {
  it('should respond with the sum of 10 and 20', async () => {
    const response = await request(app).get('/sum');
    expect(response.status).toBe(200);
    expect(response.text).toBe('30');
  });
});