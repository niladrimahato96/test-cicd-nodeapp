function sendResponse(res, status, message) {
    res.status(status).send(message);
}
  
module.exports = { sendResponse };