const express = require('express');
const { sendResponse } = require('./utils');
const app = express();
const sum = require('./testapp-cicd-submodule/helpers/sum')

// Additional routes and logic to test
app.get('/active', (req, res) => sendResponse(res, 200, 'Hello, world!'));

app.get('/error', (req, res) => sendResponse(res, 500, 'Internal Server Error.'));

app.get('/sum', (req, res) => {

    
    const value = sum(10, 20);

    
    return sendResponse(res, 200, `${value}`);
})

module.exports = app;